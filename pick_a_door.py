#!/usr/bin/env python

# pylint: disable=no-member

import sys
import random
import pygame

pygame.init()
pygame.font.init()


def check_which_door(mousex, mousey):
    if DOOR_ONE_POS[0] < mousex < DOOR_ONE_POS[0] + DOOR_WIDTH:
        if DOOR_ONE_POS[1] < mousey < DOOR_ONE_POS[1] + DOOR_HEIGHT:
            return 1
    elif DOOR_TWO_POS[0] < mousex < DOOR_TWO_POS[0] + DOOR_WIDTH:
        if DOOR_TWO_POS[1] < mousey < DOOR_TWO_POS[1] + DOOR_HEIGHT:
            return 2
    elif DOOR_THREE_POS[0] < mousex < DOOR_THREE_POS[0] + DOOR_WIDTH:
        if DOOR_THREE_POS[1] < mousey < DOOR_THREE_POS[1] + DOOR_HEIGHT:
            return 3
    else:
        return None

def choose_reveal_door(winning_door, door_clicked):
    # choose which door to reveal using sets
    # a set will ignore duplicates
    # create a set with all possible doors
    reveal_options = set([1, 2, 3])
    # create a set of doors that cannot be revealed
    can_not_reveal = set([door_clicked, winning_door])
    # remove can_not_reveal from reveal_options
    reveal_options.difference_update(can_not_reveal)
    # randomly choose door to reveal
    reveal_door = random.choice(list(reveal_options))
    return reveal_door

def find_switch_door(door_clicked, reveal_door):
    # find door to switch to
    can_not_switch = set([reveal_door, door_clicked])
    switch_door_set = set([1, 2, 3]).difference(can_not_switch)
    switch_door = list(switch_door_set)[0]
    return switch_door

def check_choice(mousex, mousey):
    if WIDTH/3-button.get_width()/2 < mousex < WIDTH/3+button.get_width()/2:
        if 410 < mousey < 410+button.get_height():
            return "stick"
    elif 2*WIDTH/3-button.get_width()/2 < mousex < 2*WIDTH/3+button.get_width()/2:
        if 410 < mousey < 410+button.get_height():
            return "switch"
    else:
        return None

def wait_for_response():
    while True:  # a never ending loop to give user time to respond
        mousex,mousey = pygame.mouse.get_pos()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pygame.quit()
                    sys.exit()
            if event.type == pygame.MOUSEBUTTONUP:
                choice = check_choice(mousex, mousey)
                if choice:
                    return choice

def wait_for_any():
    while True:  # a never ending loop to give user time to respond
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pygame.quit()
                    sys.exit()
            if event.type == pygame.MOUSEBUTTONUP:
                return

# make cursor transparent; use for touch screen
#pygame.mouse.set_cursor((8,8),(0,0),(0,0,0,0,0,0,0,0),(0,0,0,0,0,0,0,0))

# define some constants and initialize some variables needed later
WIDTH = 800
HEIGHT = 480
DISPLAY_SIZE = (WIDTH, HEIGHT)
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
YELLOW = (255, 255, 0)
ORANGE = (255, 128, 0)
BLUE = (0, 0, 255)
DOOR_COLOR = (165, 42, 42)
BUTTON_COLOR = (165, 42, 42)
LARGE_FONT = pygame.font.SysFont("monospace", 50, bold=True)
MEDIUM_FONT = pygame.font.SysFont("monospace", 30, bold=True)
SMALL_FONT = pygame.font.SysFont("monospace", 24, bold=True)
PRIZE_FONT = pygame.font.SysFont(None, 26)
DOOR_WIDTH = 100
DOOR_HEIGHT = 200
DOOR_SIZE = (DOOR_WIDTH, DOOR_HEIGHT)
DOOR_ONE_POS = (WIDTH/4 - DOOR_WIDTH/2, HEIGHT/2 - DOOR_HEIGHT/2)  # 1/4 from left, 1/2 from top
DOOR_TWO_POS = (WIDTH/2 - DOOR_WIDTH/2, HEIGHT/2 - DOOR_HEIGHT/2)  # 1/2 from left, 1/2 from top
DOOR_THREE_POS = (3*WIDTH/4 - DOOR_WIDTH/2, HEIGHT/2 - DOOR_HEIGHT/2)  # 3/4 from left, 1/2 from top
mousex = None
mousey = None
new_game = True

screen = pygame.display.set_mode((WIDTH,HEIGHT), 0, 32)  # the main screen
#screen = pygame.display.set_mode((0,0), pygame.FULLSCREEN)  # the main screen

# create surfaces for the door numbers
one = LARGE_FONT.render("1", True, WHITE)
two = LARGE_FONT.render("2", True, WHITE)
three = LARGE_FONT.render("3", True, WHITE)

# calculate position of numbers on the doors
num_pos = (DOOR_WIDTH/2-three.get_width()/2,
           DOOR_HEIGHT/4-three.get_height()/2)

# create surfaces that represent the closed doors
door_one = pygame.Surface(DOOR_SIZE)
door_one.fill(DOOR_COLOR)
pygame.draw.circle(door_one, YELLOW, (7*DOOR_WIDTH/8, DOOR_HEIGHT/2), 4)
door_one.blit(one, num_pos)

door_two = pygame.Surface(DOOR_SIZE)
door_two.fill(DOOR_COLOR)
pygame.draw.circle(door_two, YELLOW, (7*DOOR_WIDTH/8, DOOR_HEIGHT/2), 4)
door_two.blit(two, num_pos)

door_three = pygame.Surface(DOOR_SIZE)
door_three.fill(DOOR_COLOR)
pygame.draw.circle(door_three, YELLOW, (7*DOOR_WIDTH/8, DOOR_HEIGHT/2), 4)
door_three.blit(three, num_pos)

# create surface that represents an open door
door_empty = pygame.Surface(DOOR_SIZE)   # set the surface width, height
door_empty.fill(DOOR_COLOR)  # fill the surface with a color for the door jam
pygame.draw.rect(door_empty, BLACK, (5,5,DOOR_WIDTH-10,DOOR_HEIGHT-10))  # a black rectangle

# create a surface to indicate an open door with a prize
door_prize = pygame.Surface(DOOR_SIZE)   # set the surface width, height
door_prize.fill(DOOR_COLOR)  # fill the surface with a color for the door jam
pygame.draw.rect(door_prize, BLACK, (5,5,90,190))  # a black rectangle
vertices = ((50,45), (70,70), (80,60), (90,70), (90,130),
            (80,140), (70,130), (50,155), (30,130), (20,140),
            (10,130), (10,70), (20,60), (30,70))
pygame.draw.polygon(door_prize, BLUE, vertices)
vertices = ((50,65), (65,80), (85,75), (85,125),
            (65,120), (50,135), (35,120), (15,125),
            (15,75), (35,80))
pygame.draw.polygon(door_prize, ORANGE, vertices)
vertices = ((10,85), (90,85), (90,115), (10,115))
pygame.draw.polygon(door_prize, DOOR_COLOR, vertices)
msg = PRIZE_FONT.render("WINNER", True, YELLOW)
door_prize.blit(msg, (DOOR_WIDTH/2-msg.get_width()/2,DOOR_HEIGHT/2-msg.get_height()/2))

# create surface for STICK and SWITCH "buttons"
button = pygame.Surface((124, 40))  # set the surface width, height
button.fill(BUTTON_COLOR)  # fill with a color

# create a black surface to cover over doors when needed
door_cover = pygame.Surface(DOOR_SIZE)  # set the surface width, height
door_cover.fill(BLACK)  # fill with black

# we don't care about MOUSEMOTION events, so prevent
# them from being added to the event queue
pygame.event.set_blocked(pygame.MOUSEMOTION)



# The main game loop
while True:  # a never ending loop to keep the program running

    mousex,mousey = pygame.mouse.get_pos()

    if new_game:
        winning_door = random.choice([1, 2, 3])

        # draw the three closed doors
        screen.fill(BLACK)
        msg = LARGE_FONT.render("Pick-A-Door", True, WHITE)
        screen.blit(msg, (WIDTH/2 - msg.get_width()/2, DOOR_ONE_POS[1]/2-msg.get_height()/2))
        screen.blit(door_one, DOOR_ONE_POS)
        screen.blit(door_two, DOOR_TWO_POS)
        screen.blit(door_three, DOOR_THREE_POS)
        pygame.display.update()
        new_game = False

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                pygame.quit()
                sys.exit()
        if event.type == pygame.MOUSEBUTTONUP:
            pygame.event.clear()
            door_clicked = check_which_door(mousex, mousey)
            if door_clicked:
                reveal_door = choose_reveal_door(winning_door, door_clicked)
                switch_door = find_switch_door(door_clicked, reveal_door)

                # create a blank screen to draw on
                screen.fill(BLACK)

                # stage some messages
                msg_string = "You selected door #%s." % door_clicked
                msg = SMALL_FONT.render(msg_string, True, WHITE)
                screen.blit(msg, (20, 30))
                msg_string = "Door #%s is revealed to be empty." % reveal_door
                msg = SMALL_FONT.render(msg_string, True, WHITE)
                screen.blit(msg, (20, 60))
                msg_string = "Do you want to STICK with door #%s or SWITCH to door #%s?" % (door_clicked, switch_door)
                msg = SMALL_FONT.render(msg_string, True, WHITE)
                screen.blit(msg, (WIDTH/2-msg.get_width()/2, 370))

                # stage the stick or switch buttons
                label = MEDIUM_FONT.render("STICK", True, WHITE)
                screen.blit(button, (WIDTH/3-button.get_width()/2,410))
                screen.blit(button, (2*WIDTH/3-button.get_width()/2,410))
                label = MEDIUM_FONT.render("STICK", True, WHITE)
                screen.blit(label, (WIDTH/3-label.get_width()/2,415))
                label = MEDIUM_FONT.render("SWITCH", True, WHITE)
                screen.blit(label, (2*WIDTH/3-label.get_width()/2,415))

                # stage the new door configuration
                if reveal_door == 1:
                    screen.blit(door_empty, DOOR_ONE_POS)
                    screen.blit(door_two, DOOR_TWO_POS)
                    screen.blit(door_three, DOOR_THREE_POS)
                if reveal_door == 2:
                    screen.blit(door_one, DOOR_ONE_POS)
                    screen.blit(door_empty, DOOR_TWO_POS)
                    screen.blit(door_three, DOOR_THREE_POS)
                if reveal_door == 3:
                    screen.blit(door_one, DOOR_ONE_POS)
                    screen.blit(door_two, DOOR_TWO_POS)
                    screen.blit(door_empty, DOOR_THREE_POS)

                # draw the new screen with the, messages, buttons, and the door revealed
                pygame.display.update()

                # wait for player to choose stick or switch
                choice = wait_for_response()

                # determine final door choice
                if choice == "switch":
                    can_not_switch = set([reveal_door, door_clicked])
                    new_door_choice = set([1, 2, 3]).difference(can_not_switch)
                    # switch door_choice
                    door_choice = list(new_door_choice)[0]
                else:
                    door_choice = door_clicked

                # cover over all text displayed earlier
                cover = pygame.Surface((WIDTH, HEIGHT/2 - DOOR_HEIGHT/2))   # set the surface width, height
                cover.fill(BLACK)  # set the color to black
                screen.blit(cover, (0, 0))  # cover area above the doors
                screen.blit(cover, (0, DOOR_ONE_POS[1] + DOOR_HEIGHT))  # cover area below the doors

                msg_string = "You chose to %s." % choice
                msg = SMALL_FONT.render(msg_string, True, WHITE)
                screen.blit(msg, (20, 30))

                msg_string = "Your final choice was door #%s." % door_choice
                msg = SMALL_FONT.render(msg_string, True, WHITE)
                screen.blit(msg, (20, 60))

                if door_choice == winning_door:
                    msg = LARGE_FONT.render("You Won!", True, WHITE)  # create a new surface with message
                else:
                    msg = LARGE_FONT.render("You Lost!", True, WHITE)  # create a new surface with message

                screen.blit(msg, (WIDTH/2 - msg.get_width()/2, HEIGHT-70-msg.get_height()))

                msg = SMALL_FONT.render("Touch or click anywhere to start the next game", True, WHITE)
                screen.blit(msg, (WIDTH/2 - msg.get_width()/2, HEIGHT-20-msg.get_height()))

                if door_choice == 1:
                    # draw a black surface over existing door
                    screen.blit(door_cover, DOOR_ONE_POS)
                    if winning_door == 1:
                        screen.blit(door_prize, DOOR_ONE_POS)
                    else:
                        screen.blit(door_empty, DOOR_ONE_POS)
                if door_choice == 2:
                    # draw a black surface over existing door
                    screen.blit(door_cover, DOOR_TWO_POS)
                    if winning_door == 2:
                        screen.blit(door_prize, DOOR_TWO_POS)
                    else:
                        screen.blit(door_empty, DOOR_TWO_POS)
                if door_choice == 3:
                    # draw a black surface over existing door
                    screen.blit(door_cover, DOOR_THREE_POS)
                    if winning_door == 3:
                        screen.blit(door_prize, DOOR_THREE_POS)
                    else:
                        screen.blit(door_empty, DOOR_THREE_POS)
                pygame.display.update()
                wait_for_any()
                new_game = True
