#!/usr/bin/env python

"""Test the Monty Hall paradox."""

import sys
import random
import easygui

def main(num_games=1000):
    """Simulate the Monty Hall Pick-A-Door Game"""
    # return a dictionary containing the number of
    # wins when sticking and switching.
    results = {}

    # run tests for switch and for stick
    # when switch = False, stick
    for switch in [True, False]:
        num_wins = 0
        for _ in range(num_games):

            # randomly select winning door
            winning_door = random.randint(1, 3)

            # have program choose door number
            door_choice = random.randint(1, 3)

            # choose which door to reveal using sets
            # a set will ignore duplicates
            # create a set with all possible doors
            reveal_options = set([1, 2, 3])
            # create a set of doors that cannot be revealed
            can_not_reveal = set([door_choice, winning_door])
            # remove can_not_reveal from reveal_options
            reveal_options.difference_update(can_not_reveal)
            # randomly choose door to reveal
            reveal_door = random.choice(list(reveal_options))

            # final door choice
            if switch:
                # find door to switch to
                can_not_switch = set([reveal_door, door_choice])
                new_door_choice = set([1, 2, 3]).difference(can_not_switch)
                # switch door_choice
                door_choice = list(new_door_choice)[0]

            if winning_door == door_choice:
                num_wins = num_wins + 1

        if switch:
            results["switch"] = num_wins
        else:
            results["stick"] = num_wins

    return results


if __name__ == "__main__":

    while 1:
        MSG = "How many games do you want to simulate?"
        TITLE = "Monty Hall Pick-A-Door Game Simulation"
        count = easygui.integerbox(MSG, TITLE, default=1000,
                                       lowerbound=1, upperbound=100000)
        if count:
            sim_results = main(count)

            # calculate percentage of games won
            percent_switch = (float(sim_results["switch"])/float(count))*100
            percent_stick = (float(sim_results["stick"])/float(count))*100

            MSG = ("Switch: %s/%s (%s%%)\n\n"
                   "Stick:  %s/%s (%s%%)") % (sim_results["switch"],
                                              count, percent_switch,
                                              sim_results["stick"],
                                              count, percent_stick)
            easygui.msgbox(MSG, title="Results")
        else:
            sys.exit(0)    # user chose Cancel
