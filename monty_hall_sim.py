#!/usr/bin/env python

"""Test the Monty Hall paradox."""

from __future__ import print_function

import random

def main(num_games=100000):
    """Simulate the Monty Hall Pick-A-Door Game"""
    # run tests for switch and for stick
    # when switch = False, stick
    for switch in [True, False]:
        num_wins = 0
        for _ in range(num_games):

            # randomly select winning door
            winning_door = random.randint(1, 3)

            # have program choose door number
            door_choice = random.randint(1, 3)

            # choose which door to reveal using sets
            # a set will ignore duplicates
            # create a set with all possible doors
            reveal_options = set([1, 2, 3])
            # create a set of doors that cannot be revealed
            can_not_reveal = set([door_choice, winning_door])
            # remove can_not_reveal from reveal_options
            reveal_options.difference_update(can_not_reveal)
            # randomly choose door to reveal
            reveal_door = random.choice(list(reveal_options))

            # final door choice
            if switch:
                # find door to switch to
                can_not_switch = set([reveal_door, door_choice])
                new_door_choice = set([1, 2, 3]).difference(can_not_switch)
                # switch door_choice
                door_choice = list(new_door_choice)[0]

            if winning_door == door_choice:
                num_wins = num_wins + 1

        # calculate percentage of games won
        percent_games_won = (float(num_wins)/float(num_games))*100

        if switch:
            print("Switch:")

        else:
            print("Stick:")
        print("    ", num_wins, "/", num_games)

        print("    ", str(percent_games_won) + "%")


if __name__ == "__main__":
    main()
